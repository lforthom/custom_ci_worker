# Sample of a custom worker image for Jenkins and GitLab-CI at CERN

This is a sample project of a customized slave image for Jenkins.
See http://cern.ch/jenkinsdocs/chapters/slaves/custom-docker.html for more information.

This project builds two docker images, based on the both CC7 and SLC6 slaves. To push
two or more images to the same repository on the [GitLab Registry](https://cern.service-now.com/service-portal/article.do?n=KB0004284),
just use different `tags` for each one.

After the images are built, one of them is used to run a job inside GitLab-CI. This demonstrates the compatibility of the
images with GitLab-CI.

Check the `.gitlab-ci.yml` of this repository for more information.

## How to use

* Fork this project
* Edit the Dockerfile (look for TODO lines) and the GitLab-CI build to install the packages
required for your Jenkins jobs
* Follow [the instructions in the CERN Jenkins documentation](http://cern.ch/jenkinsdocs/chapters/slaves/custom-docker.html)
to add this custom image to your Jenkins instance if necessary.

## Note about ENTRYPOINT in custom CI worker images

Docker images must [start a JNLP client to be usable as Jenkins slaves](https://github.com/jenkinsci/kubernetes-plugin/blob/master/README.md#jenkins-kubernetes-plugin).
The [base ci-worker images](https://gitlab.cern.ch/ci-tools/ci-worker) define an entry point that supports the following use cases:
* use in GitLab CI
* use with Jenkins, in which case a JNLP agent is started
* interactive use with e.g. `docker run --interactive --tty <image> bash`

For the custom worker image to be compatible with Jenkins, it is recommended not to 
[set a new ENTRYPOINT](https://docs.docker.com/engine/reference/builder/#entrypoint)
and keep using the base ci-worker image's entry point. 

If a custom entry point is added nevertheless, make sure it eventually executes the base image's entrypoint
in order to start the JNLP client when used by Jenkins. For instance:

```bash
#!/usr/bin/bash

# Perform some custom Entrypoint steps...

# Execute parent image's entrypoint 
exec /usr/local/bin/run-ci-worker "$@"
```