# Sample Dockerfile to build a custom Jenkins slave image to be used
# with CERN Jenkins instances (cf. http://cern.ch/jenkinsdocs)

# Start from the base SLC6 or CC7 slave images
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cs8
# Use tag 'slc6' instead of 'cc7' for SLC6
# The FROM statement can be overriden in the GitLab-CI build

RUN yum install -y epel-release
# install custom packages
### TODO: set the list of packages as necessary
RUN yum group install -y "Development Tools"
RUN yum install -y cmake3
RUN yum install -y gsl gsl-devel libgfortran gcc-gfortran
#RUN yum install -y pythia*
#RUN yum install -y lhapdf lhapdf-devel
#RUN yum install -y muParser muParser-devel
RUN yum install -y boost-devel
RUN yum install -y root
RUN yum install -y qt*-devel
RUN yum install -y python3-devel
RUN yum clean all

